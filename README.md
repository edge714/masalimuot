# masalimuot
- curated bookmarks for a Philippine context
   - government websites
   - software development
   - information technology
   - privacy and security
   - cryptography

# kinaadman
- other bookmarks and cheat sheet
   - reference library
   - mnemonic devices
   - constants

# configuration files
- Firefox _user.js_
   - hardened Firefox configuration
   - copy to Firefox profile
- sample GnuPG configuration files
   - _gpg.conf_
   - _dirmngr.conf_
   - _gpg-agent.conf_
