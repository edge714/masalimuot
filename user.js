// --------------------------------------------------------
// Hardended Firefox User Preferences
// last update: 2021 Feb 19
// --------------------------------------------------------

/* media */
user_pref("media.autoplay.default", 5);
user_pref("media.autoplay.allow-muted", false);
user_pref("media.autoplay.enabled.user-gestures-needed", true);
user_pref("media.autoplay.ask-permission", true);
user_pref("media.peerconnection.enabled", true);
user_pref("media.navigator.enabled", false);
user_pref("media.navigator.video.enabled", false);
user_pref("media.eme.enabled", false);
user_pref("media.getusermedia.screensharing.enabled", false);
user_pref("media.getusermedia.audiocapture.enabled", false);
user_pref("pdfjs.disabled", true);

/* pocket */
user_pref("extensions.pocket.api", "");
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.pocket.oAuthConsumerKey", "");
user_pref("extensions.pocket.site", "");

/* DNS over HTTPS */
// disable DNS over HTTPS if your VPN is always on
user_pref("network.trr.mode", 5); // (2,3,5) use 5 to disable
// user_pref("network.trr.bootstrapAddress", "8.8.8.8");   // Google
// user_pref("network.trr.bootstrapAddress", "1.1.1.1");   // Cloudflare
user_pref("network.trr.bootstrapAddress", "9.9.9.9");   // Quad 9
// user_pref("network.trr.custom_uri", "https://dns.google/dns-query");           // Google
// user_pref("network.trr.custom_uri", "https://cloudflare-dns.com/dns-query");   // Cloudflare
user_pref("network.trr.custom_uri", "https://dns.quad9.net/dns-query");        // Quad 9
user_pref("network.trr.request_timeout_ms", 3000);
user_pref("network.trr.max-fails", 13);

/* privacy fixes */
user_pref("network.security.esni.enabled", true);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.prefetch-next", false);
user_pref("network.IDN_show_punycode", true);
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.disk_cache_ssl", false);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.send_pings", false);
user_pref("browser.send_pings.max_per_link", 0);
user_pref("browser.send_pings.require_same_host", true);
user_pref("dom.event.clipboardevents.enabled", false);
user_pref("dom.battery.enabled", false);
user_pref("dom.telephony.enabled", false);
user_pref("dom.vibrator.enabled", false);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.firstparty.isolate", true);
user_pref("privacy.resistFingerprinting", false); // should be TRUE
user_pref("privacy.trackingprotection.cryptomining.enabled", true);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.pbmode.enabled", true);
user_pref("privacy.partition.network_state", true);
user_pref("privacy.partition.network_state.connection_with_proxy", true);
user_pref("geo.enabled", false);
user_pref("geo.wifi.uri", "");
user_pref("geo.provider.network.url", "");
user_pref("device.sensors.enabled", false);

/* security fixes - disable weak ciphers */
user_pref("security.ssl3.rsa_null_sha", false);
user_pref("security.ssl3.rsa_null_md5",	false);
user_pref("security.ssl3.ecdhe_rsa_null_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_null_sha", false);
user_pref("security.ssl3.ecdh_rsa_null_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_null_sha", false);
user_pref("security.ssl3.rsa_seed_sha", false);

user_pref("security.ssl3.rsa_rc4_40_md5", false);
user_pref("security.ssl3.rsa_rc2_40_md5", false);
user_pref("security.ssl3.rsa_1024_rc4_56_sha", false);

user_pref("security.ssl3.ecdh_ecdsa_rc4_128_sha", false);
user_pref("security.ssl3.ecdh_rsa_rc4_128_sha",	false);
user_pref("security.ssl3.ecdhe_ecdsa_rc4_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_rc4_128_sha", false);
user_pref("security.ssl3.rsa_rc4_128_md5", false);
user_pref("security.ssl3.rsa_rc4_128_sha", false);
user_pref("security.tls.unrestricted_rc4_fallback", false);

user_pref("security.ssl3.dhe_dss_des_ede3_sha", false);
user_pref("security.ssl3.dhe_rsa_des_ede3_sha",	false);
user_pref("security.ssl3.ecdh_ecdsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdh_rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_rsa_des_ede3_sha", false);
user_pref("security.ssl3.rsa_des_ede3_sha", false);
user_pref("security.ssl3.rsa_fips_des_ede3_sha", false);

user_pref("security.ssl3.ecdh_rsa_aes_256_sha",	false);
user_pref("security.ssl3.ecdh_ecdsa_aes_256_sha", false);
user_pref("security.ssl3.rsa_camellia_256_sha",	false);

user_pref("security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256", true); // 0xc02b
user_pref("security.ssl3.ecdhe_rsa_aes_128_gcm_sha256",	true); // 0xc02f

user_pref("security.ssl3.ecdhe_ecdsa_chacha20_poly1305_sha256",	true);
user_pref("security.ssl3.ecdhe_rsa_chacha20_poly1305_sha256", true);

user_pref("security.ssl3.dhe_rsa_camellia_256_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_256_sha", false);

user_pref("security.ssl3.dhe_dss_aes_128_sha", false);
user_pref("security.ssl3.dhe_dss_aes_256_sha", false);
user_pref("security.ssl3.dhe_dss_camellia_128_sha", false);
user_pref("security.ssl3.dhe_dss_camellia_256_sha",	false);

/* security fixes - TLS/SSL */
user_pref("security.ssl.require_safe_negotiation", true);
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
user_pref("security.ssl.enable_false_start", false);
user_pref("security.ssl.disable_session_identifiers", true);
user_pref("security.tls.enable_0rtt_data", false);
user_pref("security.tls.version.min", 3);
user_pref("security.tls.version.max", 4);
user_pref("security.csp.enable", true);
user_pref("security.sri.enable", true);
user_pref("dom.security.https_only_mode", true);

/* paranoia */
user_pref("permissions.default.camera", 2);
user_pref("permissions.default.geo", 2);
user_pref("permissions.default.microphone", 2);
user_pref("signon.autofillForms", false);
user_pref("signon.rememberSignons", false);
user_pref("beacon.enabled", false);
user_pref("devtools.debugger.remote-enabled", false);
user_pref("devtools.debugger.force-local", true);
